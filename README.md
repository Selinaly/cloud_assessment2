# Cloud Assessment 2
---

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.
***
### Links to working apps:
http://trimsjenkins.academy.labs.automationlogic.com:8080

http://trims-php.academy.labs.automationlogic.com/

http://trims-petclinic.academy.labs.automationlogic.com/

http://trims-wordpress.academy.labs.automationlogic.com/login

## Table of Contents
---
<p></p>
1. Script Names and Descriptions
<p></p>
		
		1.1. PetClinic
	
		1.2. PHP
	
		1.3. Wordpress
	
<p></p>	
2. Order Of Operation To Manually Launch PetClinic App
<p></p>

		2.1. Creating An RDS DB Instance To Store PetClinic Data and A Main Webserver To Populate The Database And Run An Instance Of PetClinic

		2.2. Creating A Second Webserver Instances To Run The App On
	
		2.3. Creating and Connecting Loadbalancer To Webserver Instances
	
		2.4. Launching the PetClinic App On Both Webservers
	
		2.5. Launching a Bastion to SSH into either Webserver or Loadbalancer
	
<p></p>	
3. PetClinic In Jenkins
<p></p>

		3.1. Launching a new PetClinic environment
	
		3.2. Launching a new webserver to connect to the loadbalancer
	
<p></p>	
4. Order Of Operation To Launch PHP App (Manually through automated scripts)
<p></p>

		4.1. Creating an RDS DB Instance to Store PHP Data
	
		4.2. Creating Webserver Instances for PHP to run on
	
		4.3. Creating a Bastion Host for PHP App
	
		4.4. Installing the PHP App On Both Webservers
	
		4.5. Creating and Connecting Loadbalancer to Webservers
	
<p></p>	
5. Order Of Operation To Launch WordPress (Manually)
<p></p>

		5.1. Launching an EC2 Webserver Instance
	
		5.2. Installing WordPress 
	
<p></p>	
6. Automation of Apps through Jenkins
<p></p>

		6.1. PHP App
	
		6.2. Wordpress App
	
	
## 1. Script Names and Descriptions
***
### PetClinic

#### **DB:**
* aws_rds_launch.sh - Launches RDS. 
* petclinic_instances_names_set_here.txt - File contains instance names that will be used as arguments when running scripts.
#### **Webserver:**
* README_manual_pc_install.md - README for manual PetClinic Install
* aws_ec2_webserver_launch.sh - Launches ec2 web-servers
* pc_fresh_db_populate.sh - Populates RDS DB through an ec2 web server 
* pc_install_dependencies_webservers.sh - Installs the dependencies required for the webservers 
* pc_launch_app_webservers.sh - Launches the PetClinic application 
* petclinic.init - Called in the install dependencies script 
#### **Loadbalancer:**
* aws_ec2_loadbalancer_launch.sh - Launches AWS instance
* pc_lb_1_setup_install_loadbalancer.sh - Provisions the AWS machine with HAProxy
* pc_lb_2_connect_webserver_loadbalancer.sh - Connects the load balancer to the 2 web servers
#### **Bastion:**
* aws_ec2_bastion_launch.sh - Launches the bastion server
***
### PHP

#### **DB:**
* README-selina.md - README for DB setup
* php_launch_RDS_script.sh - Launches RDS server
#### **Webserver:**
* README_php.md - README for PHP app setup
* php_instance_launch.sh - Launches AWS instance
* provision_php_aws.sh - Provisions the AWS machine with Apache, PHP & PHP app & connects to DB
* html_button.html - HTML Button code
#### **Loadbalancer:**
* php_loadbalancer_launch.sh - Launches AWS instance
* php_install_loadbalancer.sh - Provisions the AWS machine with HAProxy
* php_connect_webserver_loadbalancer.sh - Connects the load balancer to the 2 web servers
#### **Bastion:**
* php_bastion_launch.sh - Launching bastion server
***
### **Wordpress**

#### **Webserver:**
* wp_launch_instance.sh - Launches AWS instance 
* wordpress_instance_name.txt - File contains instance name that will be used  
#### **Provisioning:**
* wp_dependencies.sh - Installs dependencies onto the wordpress server 
* wp.init - Called in the wp_dependencies.sh script

## 2. Order Of Operation To Manually Launch PetClinic App
***

Step by step instructions to launch the PetClinic app with fully automated scripts.

### Creating An RDS DB Instance To Store PetClinic Data and A Main Webserver To Populate The Database And Run An Instance Of PetClinic

Let's call our database: `trims-pc-database` 
(The Database's Username and Password are set in the script `aws_rds_launch.sh')
And our main webserver: `trims-pc-webserver1`

1. `./aws_rds_launch.sh trims-pc-databse`
2. `./aws_ec2_webserver_launch.sh trims-pc-webserver1`
3. `./pc_install_dependencies_webservers.sh trims-pc-webserver1`
4. `./pc_fresh_db_populate.sh trims-pc-webserver1 trims-pc-database`

The main webserver should be connected to the database, with database populated with fresh PetClinic Data.
**WARNING:** Understand that running the pc_fresh_db_populate.sh with the same "trims-pc-database" as the second argument **will overwrite** any data added later.

### Creating A Second Webserver Instances To Run The App On

Let's create a second webserver called:`trims-pc-webserver2`

1. `./aws_ec2_webserver_launch.sh trims-pc-webserver2`
2. `./pc_install_dependencies_webservers.sh trims-pc-webserver2`

We will launch the app after creating a loadbalancer and connecting it with the two webservers

### Creating and Connecting Loadbalancer To Webserver Instances

Let's call our loadbalancer: `trims-pc-loadbalancer`
Which connects two our two webservers: `trims-pc-webserver1` and `trims-pc-webserver2`

1. `./aws_ec2_loadbalancer_launch.sh trims-pc-loadbalancer`
3. `./pc_lb_1_setup_install_loadbalancer.sh trims-pc-loadbalancer`
4. `./pc_lb_2_connect_webserver_loadbalancer.sh trims-pc-webserver1 trims-pc-loadbalancer`
5. `./pc_lb_2_connect_webserver_loadbalancer.sh trims-pc-webserver2 trims-pc-loadbalancer`

### Launching the PetClinic App On Both Webservers

We need to specify the Webserver name AND the Database we want to connect it to

1. `./pc_launch_app_webservers.sh trims-pc-webserver1 trims-pc-database`
2. `./pc_launch_app_webservers.sh trims-pc-webserver2 trims-pc-database`

### Launching a Bastion to SSH into either Webserver or Loadbalancer

A bastion instance is the only way to SSH into either webservers or the loadbalancer to make direct changes.
This bastion will be able to access all instances in the petclinic project.
In order to launch a bastion (e.g. `trims-pc-bastion`):

1. `./aws_ec2_bastion_launch.sh trims-pc-bastion`

You must first `scp` the webserver/loadbalancer key into the Bastion and then `ssh` into the bastion to be able to `ssh` into the desired webserver/loadbalancer instance.

---
## PetClinic In Jenkins

In using Jenkins to launch PetClinic it's important to understand the jobs' build triggers.

`pc-1-launch-rds`

`pc-2-launch-bastion`

`pc-3-launch-db-populating-mainwebserver -> pc-3a-install-dependencies-and-populate-db-through-mainwebserver`

`pc-4-launch-loadbalancer -> pc-4a-loadbalancer-dependency-install-and-connect-to-mainwebserver -> pc-4b-run-webapp`

`pc-5-launch-apponly-webserver -> pc-5a-install-dependencies-on-apponly-webserver -> pc-5b-connect-to-loadbalancer -> pc-5c-run-webapp`


In separating our concerns as much as possible we have greater control of how we want to update our PetClinic environment. It's also important to note that the instances name are set in the text file `petclinic_instances_names_set_here.txt`
Also it is important to note what makes a main webserver is a webserver which **connects and fresh populates the database**. That is to say **there should be only one main webserver per environment**.
This also means that when launching new webservers to connect to the loadbalancer, **the main webserver name should not be changed in `petclinic_instances_names_set_here.txt`**.

### Launching a new PetClinic environment

1. You must set all your instance names in the relevant place in `petclinic_instances_names_set_here.txt`
2. You then execute the builds in their order.

### Launching a new webserver to connect to the loadbalancer

In order to create a new webserver and to connect it to the loadbalancer of the environment, follow these steps.
Extra webservers are launched one at a time, and so it's important that **the main webserver name should not be changed in `petclinic_instances_names_set_here.txt`**.

1. Choose the name of the extra webserver in `petclinic_instances_names_set_here.txt` on line 8.
2. Run `pc-5-launch-apponly-webserver` and wait. 
3. You should have a new webserver connected to the load balancer.


## Order Of Operation To Launch PHP App (Manually through automated scripts)
***

Step by step instructions to launch the PHP app with fully automated scripts.

### **1.** Creating an RDS DB Instance to Store PHP Data

RDS already automatically back ups databases, you can edit the frequency and timing to your specifications.

Database name:
`trims-php-database`


```
$ ./php_launch_RDS_script.sh <Database name>

$ ./php_launch_RDS_script.sh trims-php-database
```

### **2.** Creating Webserver Instances for PHP to run on

Two instances created:

1. `trims-php-ws`
2. `trims-php-ws2`


```
$ ./php_instance_launch.sh <Instance Name>

$ ./php_instance_launch.sh trims-php-ws
$ ./php_instance_launch.sh trims-php-ws2
```
### **3.** Creating a Bastion Host for PHP App

Provides access to a PHP webserver from an external network, adding another layer of security by minimising the chances of penetartion.

Bastion Host name:
`trims-php-bastion`

```
$ ./php_bastion_launch.sh <Bastion Host Name>

$ ./php_bastion_launch.sh trims-php-bastion
```

### **4.** Installing the PHP App On Both Webservers

We need to specify the names of the Webserver AND the Database we want to connect it to. 


```
$ ./provision_php_aws.sh <Webserver Name> <Database Name>

$ ./provision_php_aws.sh trims-php-ws trims-php-database
$ ./provision_php_aws.sh trims-php-ws2 trims-php-database
```
### **5.** Creating and Connecting Loadbalancer to Webservers

Manages the flow of information between the PHP app webserver and an endpoint device. 

Loadbalancer name: `trims-php-lb`

Creating the insatnce and installing the loadbalancer.


```
$ ./php_loadbalancer_launch.sh trims-php-lb
$ sleep 60
$ ./php_install_loadbalancer.sh trims-php-lb
```
Connecting the loadbalancer to both webservers running the PHP app.

```
$ ./php_connect_webserver_loadbalancer.sh <Webserverf Name> <Loadbalancer Name>

$ ./php_connect_webserver_loadbalancer.sh trims-php-ws trims-php-lb
$ ./php_connect_webserver_loadbalancer.sh trims-php-ws2 trims-php-lb
```

## Order Of Operation To Launch WordPress (Manually)
***

Step by step instructions to launch the WordPress app with fully automated scripts.

### 1. Launching an EC2 Webserver Instance
Instance Name: `trims_wordpress_test`

One instance is created by passing the name as an argument when running the launch script.

`./wp_launch_instance.sh trims_wordpress_test`


### 2. Installing WordPress 
Dependencies are installed to the webserver.

`./wp_dependencies.sh trims_wordpress_test`

## Automation of Apps through Jenkins
***

### **PHP App**

Top launch a new PHP app through jenkins, you would have to change the names of each variable in the `Execute Shell` section.


Enter database name.

1. rds-php-launch


Enter instance names for webserver 1 and 2.

2. ec2-launch-php


Enter name for bastion host server.

3. php_bastion_launch


Direct variables to correct webservers.

4. php-app-launch
5. php-loadbalancer-launch

### **WordPress App**

1. wp_launch_instance
2. wp_dependencies
