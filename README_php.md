# Cloud Assessment 2
# PHP Application Setup

## Set up 2 EC2 instances- Amazon Linux Server
- This has been automated using a script (see php_instance launch.sh)

### Installation of PHP, mysql (MariaDB), and Apache

```bash
sudo yum update -y

sudo amazon-linux-extras install -y php7.2 lamp-mariadb10.2-php7.2

sudo yum install -y httpd mariadb-server

sudo yum -y install httpd
if rpm -qa | grep "^httpd-[0-9]" >/dev/null 2>&1
then
  sudo systemctl start httpd
  sudo systemctl enable httpd
else
    exit 1
fi
```
### Set file permissions for apache

```bash
sudo usermod -a -G apache ec2-user
# exit then ssh back in if running from cmd line
sudo chown -R ec2-user:apache /var/www #Change the group ownership of /var/www and its contents to the apache group.
sudo chmod 2775 /var/www && find /var/www -type d -exec sudo chmod 2775 {} \; #To add group write permissions and to set the group ID on future subdirectories, change the directory permissions of /var/www and its subdirectories.

find /var/www -type f -exec sudo chmod 0664 {} \; #To add group write permissions, recursively change the file permissions of /var/www and its subdirectories
```

### Installation of git and cloning the PHP app repo
```bash
sudo yum -y install git

yes | git clone https://Tanya-Moller@bitbucket.org/Tanya-Moller/janglefett-simple_academy_php_app_.git
```

### Move the files to /var/www/html
```bash
sudo cp -a janglefett-simple_academy_php_app_/. /var/www/html
rm -rf ~/janglefett-simple_academy_php_app_
```


### Change the config.php file to allow connection to the DB Server
```bash
cd /var/www/html

sudo sed -i "s,$host       = getenv(\"DBHOST\");,$host       = \"trims-database-php.c6jtmnlypkzc.eu-west-1.rds.amazonaws.com\";," config.php
sudo sed -i "s,$username   = getenv(\"DBUSER\");,$username   = \"admin\";," config.php
sudo sed -i "s,$password   = getenv(\"DBPASS\");,$password   = \"assessment2\";," config.php
sudo sed -i "s,$dbname     = \"phpapp\";,$dbname     = \"PHPDatabase6\";," config.php
```

#### Need permission from DB (private IP) before install.

### Install the PHP App & Create the DB.

```bash
php install.php

```












