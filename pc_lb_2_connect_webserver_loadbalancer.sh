#!/bin/bash

WEBSERVER_FILE=$1
LOADBALANCER_NAME=$2
TXT_FILE_SUFFIX='-details.txt'
CNF_FILE_SUFFIX='.cnf'



if (( $# < 2 )) 
then
  echo "You must supply the Webserver Name AND Loadbalancer Name (in that order)" 1>&2  ## (1>&2): map stdout to stderr
  exit 1 ## exit 1 (anything that not 0) is recognized a fail exit  

fi

if [ -f ${WEBSERVER_FILE}${TXT_FILE_SUFFIX} ]
then
   echo "Starting webserver: $WEBSERVER_FILE"
else
  ## searches for the automatically created file which has instance name in the file
   echo "$WEBSERVER_FILE instance does not exist."
   exit 1
fi

if [ -f ${LOADBALANCER_NAME}${TXT_FILE_SUFFIX} ]
then
   echo "Connecting to Loadbalancer: $LOADBALANCER_NAME"
else
  ## searches for the automatically created file which has instance name in the file
   echo "$LOADBALANCER_NAME instance does not exist."
   exit 1
fi

## saves the second line (by design it is the public ip) and assigns it to hostname as usual
hostname=$(sed -n '2p' ${LOADBALANCER_NAME}${TXT_FILE_SUFFIX})


scp -o StrictHostKeyChecking=no $WEBSERVER_FILE$TXT_FILE_SUFFIX ec2-user@$hostname:/tmp/webserver_info.txt
ssh -o StrictHostKeyChecking=no ec2-user@$hostname ' 


WEBSERVER_IP=$(sed -n "3p" /tmp/webserver_info.txt)
WEBSERVER_ID=$(sed -n "1p" /tmp/webserver_info.txt)


sudo sh -c "echo \"   server server_$WEBSERVER_ID $WEBSERVER_IP:8080 check\" >>/etc/haproxy/haproxy.cfg"

sudo systemctl restart haproxy


exit

'

