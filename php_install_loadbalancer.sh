#!/bin/bash

## To run script, the argument must be the name of the instance
## e.g. the command looks like: $./pc_1_db_setup_install_dependencies.sh trims-pc-webserver

LOADBALANCER_NAME=$1
TXT_FILE_SUFFIX='-details.txt'

if [ -f $LOADBALANCER_NAME$TXT_FILE_SUFFIX ]
then
   echo "Starting Loadbalancer Setup: $LOADBALANCER_NAME"
else
  ## searches for the automatically created file which has instance name in the file
   echo "$LOADBALANCER_NAME instance does not exist."
   exit 1
fi

## saves the second line (by design it is the public ip) and assigns it to hostname as usual
hostname=$(sed -n '2p' $LOADBALANCER_NAME$TXT_FILE_SUFFIX)


ssh -o StrictHostKeyChecking=no ec2-user@$hostname ' 


##Installation 

sudo yum install gcc pcre-devel tar make -y

wget http://www.haproxy.org/download/2.0/src/haproxy-2.0.7.tar.gz -O ~/haproxy.tar.gz

tar xzvf ~/haproxy.tar.gz -C ~/

cd ~/haproxy-2.0.7

make TARGET=linux-glibc

sudo make install

sudo mkdir -p /etc/haproxy
sudo mkdir -p /var/lib/haproxy 
sudo touch /var/lib/haproxy/stats

sudo ln -s /usr/local/sbin/haproxy /usr/sbin/haproxy

sudo cp ~/haproxy-2.0.7/examples/haproxy.init /etc/init.d/haproxy
sudo chmod 755 /etc/init.d/haproxy
sudo systemctl daemon-reload

sudo chkconfig haproxy on

sudo useradd -r haproxy

sudo sh -c "echo \"
global
   log /dev/log local0
   log /dev/log local1 notice
   chroot /var/lib/haproxy
   stats timeout 30s
   user haproxy
   group haproxy
   daemon

defaults
   log global
   mode http
   option httplog
   option dontlognull
   timeout connect 5000
   timeout client 50000
   timeout server 50000

frontend http_front
   bind *:80
   stats uri /haproxy?stats
   default_backend http_back

backend http_back
   balance roundrobin

\" >/etc/haproxy/haproxy.cfg"



exit 0

'

