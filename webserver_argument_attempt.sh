#!/bin/bash 

export AWS_PROFILE=Academy 

if (( $#> 0)) # Evaluates a condition (returns true/false/binary)
then
    Name=$1 #Instance Name 
    Key=$2 #Adding another tag so that we can retrieve the instance_id
else 
    echo "You must supply a name and separate tag-key for your server" 1>&2 # Move and merge standard output to standard error. 
    exit 1 # Identify it as a failure 
fi

# Launch instance 

aws ec2 run-instances --image-id ami-0ffea00000f287d30 --count 1 --instance-type t2.micro --key-name MohamedNasrKey --security-group-ids sg-01f897dc42d10c7de sg-0bc487735f9c682d7 --subnet-id subnet-7bddfc1d --region eu-west-1 --tag-specifications 'ResourceType=instance,Tags=[{Key=Name,Value='$Name'},{Key='$Key',Value=" "}]'

# Retrieve info 

Instance_ID=`aws --region eu-west-1 ec2 describe-instances --filters "Name=instance-state-name,Values=pending" Name=tag-key,Values=$Key --query 'Reservations[*].Instances[*].[InstanceId]' --output text` 

echo $Instance_ID

Private_IP=`aws --region eu-west-1 ec2 describe-instances --filters "Name=instance-id,Values=$Instance_ID" --query 'Reservations[*].Instances[*].[PrivateIpAddress]' | grep -vE '\[|\]' | awk -F'"' '{ print $2 }'`       

Public_IP=`aws --region eu-west-1 ec2 describe-instances --filters "Name=instance-id,Values=$Instance_ID" --query 'Reservations[*].Instances[*].[PublicIpAddress]' | grep -vE '\[|\]' | awk -F'"' '{ print $2 }'`

echo $Private_IP

a='/32'
echo $a

aws --region eu-west-1 ec2 authorize-security-group-ingress --group-name sc-trims-pc-db --protocol tcp --port 3306 --cidr $Private_IP$a

ssh -o StrictHostKeyChecking=no -i ~/.ssh/MohamedNasrKey.pem ec2-user@$Public_IP  '

 
sudo yum -y install git 
git clone https://mnasr6263@bitbucket.org/JangleFett/petclinic.git


sudo sed -i "spring.datasource.url=jdbc:mysql://trims-pc-db2.c6jtmnlypkzc.eu-west-1.rds.amazonaws.com/petclinic," ~/petclinic/src/main/resources/application.properties
sudo sed -i "spring.datasource.username=trimsadmin," ~/petclinic/src/main/resources/application.properties
sudo sed -i "spring.datasource.password=trimspwdpwd123," ~/petclinic/src/main/resources/application.properties



sudo yum install -y httpd 

sudo service httpd start 

sudo systemctl start httpd 



sudo curl -L -C - -b "oraclelicense=accept-securebackup-cookie" -O 'http://download.oracle.com/otn-pub/java/jdk/8u131-b11/d54c1d3a095b4ff2b6607d096fa80163/jdk-8u131-linux-x64.tar.gz'

sudo mkdir /usr/lib/jvm/
cd /usr/lib/jvm
sudo tar -xvzf ~/jdk-8u131-linux-x64.tar.gz
sudo sh -c "echo PATH=$PATH:/usr/lib/jvm/jdk1.8.0_131/bin:/usr/lib/jvm/jdk1.8.0_131/db/bin:/usr/lib/jvm/jdk1.8.0_131/jre/bin >>/etc/environment"
sudo sh -c "echo J2SDKDIR=\"/usr/lib/jvm/jdk1.8.0_131\" >>/etc/environment"
sudo sh -c "echo J2REDIR=\"/usr/lib/jvm/jdk1.8.0_131/jre\" >>/etc/environment"
sudo sh -c "echo JAVA_HOME=\"/usr/lib/jvm/jdk1.8.0_131\" >>/etc/environment"
sudo sh -c "echo DERBY_HOME=\"/usr/lib/jvm/jdk1.8.0_131/db\" >>/etc/environment"
sudo update-alternatives --install "/usr/bin/java" "java" "/usr/lib/jvm/jdk1.8.0_131/bin/java" 0
sudo update-alternatives --install "/usr/bin/javac" "javac" "/usr/lib/jvm/jdk1.8.0_131/bin/javac" 0
sudo update-alternatives --set java /usr/lib/jvm/jdk1.8.0_131/bin/java
sudo update-alternatives --set javac /usr/lib/jvm/jdk1.8.0_131/bin/javac
update-alternatives --list java
update-alternatives --list javac


cd /opt
sudo wget https://downloads.apache.org/maven/maven-3/3.8.1/binaries/apache-maven-3.8.1-bin.tar.gz
sudo tar -xvzf apache-maven-3.8.1-bin.tar.gz
sudo sh -c "echo M2_HOME=\"/opt/apache-maven-3.8.1\" >>/etc/environment"
sudo sh -c "echo PATH=$PATH:/opt/apache-maven-3.8.1/bin >>/etc/environment"
#USE SED TO REPLACE PATH LIKE IN JSPAINT.SH LIKE THE BELOW ONE
sudo sed -i "s,PATH=\$PATH,PATH=$PATH:/opt/apache-maven-3.8.1/bin," /etc/environment
sudo update-alternatives --install "/usr/bin/mvn" "mvn" "/opt/apache-maven-3.8.1/bin/mvn" 0
sudo update-alternatives --set mvn /opt/apache-maven-3.8.1/bin/mvn
sudo wget https://raw.github.com/dimaj/maven-bash-completion/master/bash_completion.bash --output-document /etc/bash_completion.d/mvn
cd


sudo amazon-linux-extras install mariadb10.5 
sudo systemctl start mariadb 


cd ~/petclinic/ #Move into petclinic directory 
mvn -Dmaven.test.skip=true package #compile the project 
java -jar target/*.jar

'