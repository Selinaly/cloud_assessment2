#!/bin/bash 

WEBSERVER_FILE=$1 ## FILL IN MANUALY FOR JENKINS PURPOSE
TXT_FILE_SUFFIX='-details.txt'
SQLPASS="wptrimspwd"


if (( $# < 1 )) 
then
  echo "You must supply the Webserver Name" 1>&2  ## (1>&2): map stdout to stderr
  exit 1 ## exit 1 (anything that not 0) is recognized a fail exit  

fi

if [ -f ${WEBSERVER_FILE}${TXT_FILE_SUFFIX} ]
then
   echo "Starting webserver: $WEBSERVER_FILE"
else
  ## searches for the automatically created file which has instance name in the file
   echo "$WEBSERVER_FILE instance does not exist."
   exit 1
fi

hostname=$(sed -n '2p' ${WEBSERVER_FILE}${TXT_FILE_SUFFIX})

scp -o StrictHostKeyChecking=no wp.init ec2-user@$hostname:wp.init 
ssh -o StrictHostKeyChecking=no ec2-user@$hostname '


# update
sudo yum update -y 

#php
sudo amazon-linux-extras install -y php7.2 

# apache
sudo yum install -y httpd 
sudo systemctl start httpd 

# mariadb
sudo amazon-linux-extras install -y mariadb10.5
sudo systemctl start mariadb

# wordpress
wget https://wordpress.org/latest.tar.gz
tar -xzf latest.tar.gz

# secure the database server 
sudo mysql_secure_installation <<EOF

n
y
wptrimspwd
wptrimspwd
y
y
y
y
EOF

# Enter sql and create user in MySQL database
sudo mysql -u root -pwptrimspwd --execute="CREATE USER \"trims\"@\"localhost\" IDENTIFIED BY \"wptrimspwd\";"
sudo mysql -u root -pwptrimspwd --execute="CREATE DATABASE wordpressdb;"
sudo mysql -u root -pwptrimspwd --execute="GRANT ALL PRIVILEGES ON wordpressdb.* TO \"trims\"@\"localhost\";"
sudo mysql -u root -pwptrimspwd --execute="FLUSH PRIVILEGES";


# create and edit wp-config.php 
cp wordpress/wp-config-sample.php wordpress/wp-config.php

# edit user info
#sudo sed -i "s/define( \"DB_NAME\", \"database_name_here\" );/define( \"DB_NAME\", \"wordpressdb\" );/" ~/wordpress/wp-config.php 
#sudo sed -i "s/define( \"DB_USER\", \"username_here\" );/define( \"DB_USER\", \"trims\" );/" ~/wordpress/wp-config.php 
#sudo sed -i -e  "s/define( \"DB_PASSWORD\", \"password_here\" );/define( \"DB_PASSWORD\", \"wptrimspwd\" );/" ~/wordpress/wp-config.php 

# edit authentication keys using init file 
sudo mv ~/wp.init /etc/init.d/wp
sudo chmod +x /etc/init.d/wp
sudo chkconfig --add wp

sudo /etc/init.d/wp start

# run wordpress at document root 
sudo cp -r wordpress/* /var/www/html/

# allow wordpress to use permalinks 
sudo sed -i "151 s/AllowOverride None/AllowOverride All/" /etc/httpd/conf/httpd.conf

# install php graphics drawing library 
sudo yum install -y php-gd

# fix file permissions for the Apache web server 
sudo chown -R apache /var/www
sudo chgrp -R apache /var/www
sudo chmod 2775 /var/www
find /var/www -type d -exec sudo chmod 2775 {} \;
find /var/www -type f -exec sudo chmod 0664 {} \;
sudo systemctl restart httpd

# run the wordpress installation script 
sudo systemctl enable httpd && sudo systemctl enable mariadb

#admin
#*qkYeqbX4dezzElEQk
'
